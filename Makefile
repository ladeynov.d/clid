APP = clid

CXX=g++
#CXX=clang++

SRC_DIR=./

CXXFLAGS=-c -Wall -std=c++2a -O3

OPTIONS += -lOpenCL

SRCS=$(wildcard ${SRC_DIR}*.cpp)
APP_OBJS = $(SRCS:.cpp=.o)

all: $(SRCS) $(APP)

$(APP): $(APP_OBJS)
	$(CXX) $(LDFLAGS) $(APP_OBJS) -o $@ $(OPTIONS)

$(SRCS).o:
	$(CXX) $(INCLUDE) $(CXXFLAGS) $< -o $@

clean:
	rm -rf ${SRC_DIR}*.o $(APP) ${SRC_DIR}*.elf ${SRC_DIR}*.gdb
