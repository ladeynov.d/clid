//MIT License

//Copyright (c) 2022 Dmitry Ladeynov

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#include <iostream>
#include <vector>
#include <string>

#define CL_HPP_HMINIMUM_OPENCL_VERSION 300
#define CL_HPP_TARGET_OPENCL_VERSION 300

#include <CL/opencl.hpp>

int main()
{
    std::cout <<"--- OpenCL Device Info ---\n";
    std::vector<cl::Platform> all_platforms;
    cl::Platform::get(&all_platforms);

    if (all_platforms.size() == 0) {
        std::cout<<" No platforms found. Check OpenCL installation!\n";
        exit(1);
    }

    for(size_t pl = 0; pl < all_platforms.size(); ++pl) {
        cl::Platform default_platform=all_platforms[pl];
        std::cout << "Platform: ID:  " << '<' << pl << '>' << '\n' <<
        std::cout << "Platform Name: " << 
                default_platform.getInfo<CL_PLATFORM_NAME>() << '\n';

        std::vector<cl::Device> all_devices;
        default_platform.getDevices(CL_DEVICE_TYPE_ALL, &all_devices);

        if(all_devices.size() == 0){
            std::cout <<"    No devices found. Check OpenCL installation!\n";
            continue;
        }

        for(size_t dv = 0; dv < all_devices.size(); ++dv) {
            cl::Device default_device=all_devices[dv];
            std::cout << "    Device ID:          " << "<" << dv << ">" << '\n';
            std::cout << "    Device Name:        " 
                    << default_device.getInfo<CL_DEVICE_NAME>() <<"\n";
            std::cout << "    Device Vendor Name: " 
                    << default_device.getInfo<CL_DEVICE_VENDOR>() <<"\n";
            std::cout << "    Device Vendor ID:   " 
                    << default_device.getInfo<CL_DEVICE_VENDOR_ID>() <<"\n";
            std::cout << "    Device OpenCL Ver.: " 
                    << default_device.getInfo<CL_DEVICE_VERSION>() <<"\n";
            std::cout << "    Device Driver Ver.: " 
                    << default_device.getInfo<CL_DRIVER_VERSION>() <<"\n";
            std::cout << "    Device Profile:     " 
                    << default_device.getInfo<CL_DEVICE_PROFILE>() <<"\n\n";
        }
    }
}
