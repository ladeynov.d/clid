## CLID

OpenCl Information Reader.

--- OpenCL Device Info ---
Platform: ID:
Platform Name:
    Device ID:
    Device Name:
    Device Vendor Name:
    Device Vendor ID:
    Device OpenCL Ver.:
    Device Driver Ver.:
    Device Profile:

### Build
```
$ make
```

